#include <ℤ/ℤ.hpp>

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <ostream>
#include <tuple>

#include <iostream>

#include <gmpxx.h>

namespace math₂ {

struct ℤ::state {
	state(mpz_class&& val) : value{std::move(val)} {}
	state(const mpz_class& val) : value{val} {}
	mpz_class value;
};

ℤ::ℤ(std::unique_ptr<state>&& state) noexcept : m_state{std::move(state)} {
	assert(m_state != nullptr);
}

ℤ::ℤ(ℤ₆₄ init) : m_state{std::make_unique<ℤ::state>(mpz_class{init})} {}

ℤ::ℤ(const std::string& s) : m_state{std::make_unique<ℤ::state>(mpz_class{s})} {}
ℤ::ℤ(const char* s) : m_state{std::make_unique<ℤ::state>(mpz_class{s})} {}

ℤ::ℤ(const ℤ& other) : m_state{std::make_unique<ℤ::state>(other.m_state->value)} {}

ℤ& ℤ::operator=(const ℤ& other) {
	this->m_state->value = other.m_state->value;
	return *this;
}

ℤ::~ℤ() = default;

auto ℤ::operator-() const -> ℤ { return ℤ{std::make_unique<ℤ::state>(-(this->m_state->value))}; }

auto operator<=>(const ℤ& lhs, const ℤ& rhs) -> std::strong_ordering {
	const auto res = cmp(lhs.m_state->value, rhs.m_state->value);
	if (res < 0) {
		return std::strong_ordering::less;
	} else if (res > 0) {
		return std::strong_ordering::greater;
	} else {
		return std::strong_ordering::equal;
	}
}

auto operator==(const ℤ& lhs, const ℤ& rhs) -> 𝔹 {
	return (lhs <=> rhs) == std::strong_ordering::equal;
}

auto operator+(const ℤ& lhs, const ℤ& rhs) -> ℤ {
	return ℤ{std::make_unique<ℤ::state>(lhs.m_state->value + rhs.m_state->value)};
}

auto operator-(const ℤ& lhs, const ℤ& rhs) -> ℤ {
	return ℤ{std::make_unique<ℤ::state>(lhs.m_state->value - rhs.m_state->value)};
}

auto operator*(const ℤ& lhs, const ℤ& rhs) -> ℤ {
	return ℤ{std::make_unique<ℤ::state>(lhs.m_state->value * rhs.m_state->value)};
}

auto operator/(const ℤ& lhs, const ℤ& rhs) -> ℤ {
	return ℤ{std::make_unique<ℤ::state>(lhs.m_state->value / rhs.m_state->value)};
}

auto operator%(const ℤ& lhs, const ℤ& rhs) -> ℤ {
	return ℤ{std::make_unique<ℤ::state>(lhs.m_state->value % rhs.m_state->value)};
}

auto pow(const ℤ& base, const unsigned long exp) -> ℤ {
	auto ret = ℤ{};
	mpz_pow_ui(ret.m_state->value.get_mpz_t(), base.m_state->value.get_mpz_t(), exp);
	return ret;
}

auto pow_m(const ℤ& base, const ℤ& exp, const ℤ& mod) -> ℤ {
	auto ret = ℤ{};
	mpz_powm(ret.m_state->value.get_mpz_t(), base.m_state->value.get_mpz_t(),
	         exp.m_state->value.get_mpz_t(), mod.m_state->value.get_mpz_t());
	return ret;
}

auto operator<<(std::ostream& stream, const ℤ& x) -> std::ostream& {
	return stream << x.m_state->value;
}

auto operator>>(std::istream& stream, ℤ& x) -> std::istream& { return stream >> x.m_state->value; }

std::string to_string(const ℤ& n) {
	auto stream = std::ostringstream{};
	stream << n;
	return stream.str();
}

} // namespace math₂

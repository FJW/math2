#ifndef UTILS_HPP
#define UTILS_HPP

#include <utility>
#include <type_traits>

namespace math₂::utils {
	
	auto narrow(ℕ₆₄ in) -> ℕ₃₂ {
		return static_cast<ℕ₃₂>(in);
	}

	auto split(ℕ₆₄ in) -> std::pair<ℕ₃₂, ℕ₃₂> {
		return {narrow(in >> 32u), narrow(in)};
	}

	auto halve_add(ℕ₃₂ lhs, ℕ₃₂ rhs) -> std::pair<𝔹, ℕ₃₂> {
		const auto [carry, res] = split(ℕ₆₄{lhs} + ℕ₆₄{rhs});
		return {carry != 0, res};
	}

	auto full_add(ℕ₃₂ lhs, ℕ₃₂ rhs, 𝔹 carry) -> std::pair<𝔹, ℕ₃₂> {
		const auto [tmp₁, carry₁] = halve_add(lhs, rhs);
		const auto [tmp₂, carry₂] = halve_add(tmp₁, carry);
		return {carry₁ or carry₂, tmp₂};
	}

	auto halve_sub(ℕ₃₂ lhs, ℕ₃₂ rhs) -> std::pair<𝔹, ℕ₃₂> {
		return { rhs > lhs, lhs - rhs};
	}

	auto full_sub(ℕ₃₂ lhs, ℕ₃₂ rhs, 𝔹 carry) -> std::pair<𝔹, ℕ₃₂> {
		const auto [tmp₁, carry₁] = halve_sub(lhs, rhs);
		const auto [tmp₂, carry₂] = halve_sub(tmp₁, carry);
		return {carry₁ or carry₂, tmp₂};
	}

	auto mul_add(ℕ₃₂ lhs, ℕ₃₂ rhs, ℕ₃₂ carry = 0) -> std::pair<ℕ₃₂, ℕ₃₂> {
		return split(ℕ₆₄{lhs} * ℕ₆₄{rhs} + ℕ₆₄{carry});
	}
}


#endif

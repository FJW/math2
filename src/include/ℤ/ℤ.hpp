#ifndef ℤ_ℤ_hpp
#define ℤ_ℤ_hpp

#include <array>
#include <vector>
#include <compare>
#include <iosfwd>
#include <memory>

#include <ℤ/types.hpp>

namespace math₂ {

class ℤ {
public:
	ℤ(ℤ₆₄ init = 0);
	explicit ℤ(const std::string&);
	explicit ℤ(const char*);

	ℤ(const ℤ&);
	ℤ(ℤ&&) noexcept = default;
	ℤ& operator=(const ℤ&);
	ℤ& operator=(ℤ&&) noexcept = default;

	~ℤ();

	auto operator+() const -> ℤ {return *this;}
	auto operator-() const -> ℤ;

	friend auto operator<=>(const ℤ& lhs, const ℤ& rhs) -> std::strong_ordering;
	friend auto operator==(const ℤ& lhs, const ℤ& rhs) -> 𝔹;

	friend auto operator+(const ℤ& lhs, const ℤ& rhs) -> ℤ;
	friend auto operator-(const ℤ& lhs, const ℤ& rhs) -> ℤ;
	
	friend auto operator*(const ℤ& lhs, const ℤ& rhs) -> ℤ;
	friend auto operator/(const ℤ& lhs, const ℤ& rhs) -> ℤ;
	friend auto operator%(const ℤ& lhs, const ℤ& rhs) -> ℤ;

	friend auto pow(const ℤ& base, unsigned long exp) -> ℤ;
	friend auto pow_m(const ℤ& base, const ℤ& exp, const ℤ& mod) -> ℤ;

	friend auto operator<<(std::ostream&, const ℤ&) -> std::ostream&;
	friend auto operator>>(std::istream&, ℤ&) -> std::istream&;


private:
	struct state;
	std::unique_ptr<state> m_state;

	ℤ(std::unique_ptr<state>&&) noexcept;
};


std::string to_string(const ℤ&);

} // namespace math₂

// UDLs should really be in the global namespace, so let's place them there:

#if defined(__GNU_C__) && !defined(__clang__)
template<char... S>
inline auto operator""_ℤ() -> math₂::ℤ {
	const auto s = std::array<char, sizeof...(S)>{{S...}};
	return math₂::ℤ{s.data()};
}
#endif

template<char... S>
inline auto operator""_ZZ() -> math₂::ℤ {
	const auto s = std::array<char, sizeof...(S)>{{S...}};
	return math₂::ℤ{s.data()};
}

#endif

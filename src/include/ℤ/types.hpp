#ifndef ℤ_types_hpp
#define ℤ_types_hpp

#include <cstdint>
#include <limits>
namespace math₂ {

using 𝔹 = bool;
using ℤ₆₄ = std::int64_t;
using ℤ₃₂ = std::int32_t;
using ℕ₆₄ = std::uint64_t;
using ℕ₃₂ = std::uint32_t;

constexpr auto ℤ₆₄_min = std::numeric_limits<ℤ₆₄>::min();
constexpr auto ℤ₆₄_max = std::numeric_limits<ℤ₆₄>::max();
constexpr auto ℤ₃₂_min = std::numeric_limits<ℤ₃₂>::min();
constexpr auto ℤ₃₂_max = std::numeric_limits<ℤ₃₂>::max();
constexpr auto ℕ₆₄_max = std::numeric_limits<ℕ₆₄>::max();
constexpr auto ℕ₃₂_max = std::numeric_limits<ℕ₃₂>::max();

} // namespace math₂

# endif

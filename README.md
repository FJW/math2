# Math²

This project provides an easy to use and nice to read frontend for GMP:

```cpp
#include <iostream>
#include <ℤ/ℤ.hpp>

int main() {
	const auto a = 12345678901234567890_ℤ;
	const auto b = 2718281828_ℤ;
	const auto c = (a + a * b) % 3141592654_ℤ;
	const auto d = math₂::ℤ{23*42};
	std::cout << pow_m(d, c, a) << '\n';
}
```

## Known issues

* GCC and Clang seem to dislike user-defined literals that start with an underscore followed by a non-ascii-letter. Workaround: `_ℤ` is disabled for GCC and `_ZZ` is provided as a fallback.
